package projeto.pucsp.tcc.alimento.oferta

import org.apache.commons.io.IOUtils
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.Header
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import reactor.core.publisher.Mono
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.time.LocalDate
import java.util.*

@ActiveProfiles("test")
@RunWith(SpringRunner::class)
@SpringBootTest
class OfertaAlimentoApplicationTests {

    @Autowired
    private val context: ApplicationContext? = null

    private var client: WebTestClient? = null

    private var clientAndServer: ClientAndServer? = null

    private val classLoader = javaClass.classLoader

    private val headerEmailEmpresarial = "email-empresarial"

    private val headIdEmpresarial = "id-empresarial"

    private val valueHeaderEmail = "email@email.com"

    private val contentType = "Content-Type"

    private val uriPublicar = "/publicar"

    private val nomeDeArquivoAlimentos = "alimentos.json"

    private val latitude = "latitude"

    private val longitude = "longitude"

    @Before
    fun setup() {

        val porta = 1080

        clientAndServer = ClientAndServer(porta)

        client = WebTestClient
                .bindToApplicationContext(this.context!!)
                .configureClient()
                .build()

    }

    @Test
    @Throws(IOException::class)
    fun publicarOfertaAlimentoComRetornoDeServicoAlimentoOkay() {

        classLoader.getResourceAsStream(nomeDeArquivoAlimentos)!!.use { inputStream ->

            val result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8)

            clientAndServer!!
                    .`when`(request().withMethod("GET").withPath("/alimentos/2"))
                    .respond(response().withHeader(Header(contentType, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.OK.value()))

            client!!
                    .post()
                    .uri(uriPublicar)
                    .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(2, 1010, 128, LocalDate.now(), LocalDate.now().plusMonths(1))), Alimento::class.java)
                    .header(headIdEmpresarial, "1")
                    .header(headerEmailEmpresarial, valueHeaderEmail)
                    .header(latitude, "-485.58627")
                    .header(longitude, "-47.475235")
                    .exchange()
                    .expectStatus().isCreated

        }

    }

    @Test
    @Throws(IOException::class)
    fun publicarOfertaAlimentoComRetornoDeServicoAlimentoOkayParaUmCodigoInvalido() {

        classLoader.getResourceAsStream(nomeDeArquivoAlimentos)!!.use { inputStream ->

            val result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8)

            clientAndServer!!
                    .`when`(request().withMethod("GET").withPath("/alimentos/1"))
                    .respond(response().withHeader(Header(contentType, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.OK.value()))

            client!!
                    .post()
                    .uri(uriPublicar)
                    .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 127, LocalDate.now(), LocalDate.now().plusMonths(2))), Alimento::class.java)
                    .header(headIdEmpresarial, "1")
                    .header(headerEmailEmpresarial, valueHeaderEmail)
                    .header(latitude, "-485.58627")
                    .header(longitude, "-47.475235")
                    .exchange()
                    .expectStatus()
                    .isNotFound
                    .expectBody()
                    .json("{\"mensagem\":\"1\",\"notificacao\":{\"codigo\":404,\"contexto\":\"Código(s) da lista alimento não encontrado(s)\"}}\n")
        }

    }

    @Test
    @Throws(IOException::class)
    fun publicarOfertaAlimentoComRetornoDeServicoAlimentoOkayParaDataFabricacaoIgualOuPosteriorADataDeValidade() {

        classLoader.getResourceAsStream(nomeDeArquivoAlimentos)!!.use { inputStream ->

            val result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8)

            clientAndServer!!
                    .`when`(request().withMethod("GET").withPath("/alimentos/2"))
                    .respond(response().withHeader(Header(contentType, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.OK.value()))

            client!!
                    .post()
                    .uri(uriPublicar)
                    .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(2, 1010, 127, LocalDate.now().plusMonths(1), LocalDate.now())), Alimento::class.java)
                    .header(headIdEmpresarial, "1")
                    .header(headerEmailEmpresarial, valueHeaderEmail)
                    .header(latitude, "-485.58627")
                    .header(longitude, "-47.475235")
                    .exchange()
                    .expectStatus()
                    .isBadRequest
                    .expectBody()
                    .jsonPath("notificacao", "{\"codigo\":4056,\"contexto\":\"Existe(m) incosistência de informações\"}")

        }

    }

    @Test
    @Throws(IOException::class)
    fun publicarOfertaAlimentoComRetornoDeServicoAlimentoRetornando404() {

        classLoader.getResourceAsStream("alimento-nao-encontrado.json")!!.use { inputStream ->

            val result = IOUtils.toString(Objects.requireNonNull(inputStream), StandardCharsets.UTF_8)

            clientAndServer!!
                    .`when`(request().withMethod("GET").withPath("/alimentos/1"))
                    .respond(response().withHeader(Header(contentType, MediaType.APPLICATION_JSON_UTF8_VALUE))
                            .withBody(result)
                            .withStatusCode(HttpStatus.NOT_FOUND.value()))

            client!!
                    .post()
                    .uri(uriPublicar)
                    .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 100, LocalDate.now(), LocalDate.now().plusDays(1))), Alimento::class.java)
                    .header(headIdEmpresarial, "1")
                    .header(headerEmailEmpresarial, valueHeaderEmail)
                    .header(latitude, "-485.58627")
                    .header(longitude, "-47.475235")
                    .exchange()
                    .expectStatus()
                    .isNotFound
                    .expectBody()
                    .json("{\n" +
                            "    \"mensagem\": \"\",\n" +
                            "    \"notificacao\": {\n" +
                            "        \"codigo\": 404,\n" +
                            "        \"contexto\": \"Código(s) da lista alimento não encontrado(s)\"\n" +
                            "    }\n" +
                            "}")
        }

    }

    @Test
    fun publicarOfertaAlimentoComRetornoDeServicoIndisponivel() {

        clientAndServer!!.stop()

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 2385, LocalDate.now().minusWeeks(4), LocalDate.now())), Alimento::class.java)
                .header(headIdEmpresarial, "1")
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(latitude, "-485.58627")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .is5xxServerError
                .expectBody()
                .json("{\"mensagem\":\"Serviço de Alimento indisponível. Tente novamente mais tarde\",\"notificacao\":{\"codigo\":500,\"contexto\":\"Serviço Indisponível\"}}\n")

    }

    @Test
    fun publicarOfertaAlimentoComDataFabricacaoPosteriorAtual() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 12384, LocalDate.now().plusDays(2), LocalDate.now().plusWeeks(4))), Alimento::class.java)
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(headIdEmpresarial, "1")
                .header(latitude, "-485.58627")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody()
                .jsonPath("notificacao", "{\"codigo\":4056,\"contexto\":\"Existe(m) incosistência de informações\"}")

    }

    @Test
    fun publicarOfertaAlimentoSemIdEmpresarial() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 12384, LocalDate.now(), LocalDate.now().plusWeeks(4))), Alimento::class.java)
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(latitude, "-485.58627")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest

    }

    @Test
    fun publicarOfertaAlimentoSemLatitude() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 12384, LocalDate.now(), LocalDate.now().plusWeeks(4))), Alimento::class.java)
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(headIdEmpresarial, "1")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest

    }

    @Test
    fun publicarOfertaAlimentoSemLongitude() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 12384, LocalDate.now(), LocalDate.now().plusWeeks(4))), Alimento::class.java)
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(headIdEmpresarial, "1")
                .header(latitude, "-485.58627")
                .exchange()
                .expectStatus()
                .isBadRequest

    }

    @Test
    fun publicarOfertaAlimentoSemEmailSocial() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, 12034, LocalDate.now().minusDays(12), LocalDate.now())), Alimento::class.java)
                .header(headIdEmpresarial, "1")
                .header(latitude, "-485.58627")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest

    }

    @Test
    fun publicarOfertaAlimentoComCodigoNegativo() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(-1, 1010, 12034, LocalDate.now().minusDays(12), LocalDate.now())), Alimento::class.java)
                .header(headIdEmpresarial, "1")
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(latitude, "-485.58627")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody()
                .jsonPath("notificacao", "{\"codigo\":4056,\"contexto\":\"Existe(m) incosistência de informações\"}")

    }

    @Test
    fun publicarOfertaAlimentoComQuantidadeNegativa() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, -1010, 12034, LocalDate.now().minusDays(12), LocalDate.now())), Alimento::class.java)
                .header(headIdEmpresarial, "1")
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(latitude, "-485.58627")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody()
                .jsonPath("notificacao", "{\"codigo\":4056,\"contexto\":\"Existe(m) incosistência de informações\"}")

    }

    @Test
    fun publicarOfertaAlimentoComNumeroLoteNegativo() {

        client!!
                .post()
                .uri(uriPublicar)
                .body<Alimento, Mono<Alimento>>(Mono.just<Alimento>(Alimento(1, 1010, -12034, LocalDate.now().minusDays(12), LocalDate.now())), Alimento::class.java)
                .header(headIdEmpresarial, "1")
                .header(headerEmailEmpresarial, valueHeaderEmail)
                .header(latitude, "-485.58627")
                .header(longitude, "-47.475235")
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody()
                .jsonPath("notificacao", "{\"codigo\":4056,\"contexto\":\"Existe(m) incosistência de informações\"}")

    }

    @After
    fun stopMockServer() {
        clientAndServer!!.stop()
    }

}
