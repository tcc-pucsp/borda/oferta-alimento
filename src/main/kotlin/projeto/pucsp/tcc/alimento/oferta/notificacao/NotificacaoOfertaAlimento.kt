package projeto.pucsp.tcc.alimento.oferta.notificacao

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import projeto.pucsp.tcc.alimento.oferta.entidade.Resposta
import projeto.pucsp.tcc.alimento.oferta.excessao.AlimentoIncorretoException
import projeto.pucsp.tcc.alimento.oferta.excessao.CodigoAlimentoIncorretoException
import projeto.pucsp.tcc.alimento.oferta.excessao.ServicoAlimentoIndisponivelException
import reactor.core.publisher.Mono

@RestControllerAdvice
class NotificacaoOfertaAlimento {

    @ExceptionHandler(CodigoAlimentoIncorretoException::class)
    fun codigoDeAlimentosIncorretos(e: CodigoAlimentoIncorretoException): ResponseEntity<Mono<Resposta>> {
        return ResponseEntity.status(e.status).body(e.resposta)
    }

    @ExceptionHandler(ServicoAlimentoIndisponivelException::class)
    fun servicoAlimentoIndisponivel(e: ServicoAlimentoIndisponivelException): ResponseEntity<Mono<Resposta>> {
        return ResponseEntity.status(e.status).body(e.resposta)
    }

    @ExceptionHandler(AlimentoIncorretoException::class)
    fun alimentoComInformacoesIncorretas(e: AlimentoIncorretoException): ResponseEntity<Mono<Resposta>> {
        return ResponseEntity.status(e.status).body(e.resposta)
    }
}
