package projeto.pucsp.tcc.alimento.oferta.propriedade

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "alimento")
class PropriedadeAlimento {

    lateinit var servicoUrl: String

}
