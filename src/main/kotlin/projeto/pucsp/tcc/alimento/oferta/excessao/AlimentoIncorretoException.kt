package projeto.pucsp.tcc.alimento.oferta.excessao

import org.springframework.http.HttpStatus
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import projeto.pucsp.tcc.alimento.oferta.entidade.Resposta
import projeto.pucsp.tcc.alimento.oferta.enumeracao.Notificacao
import reactor.core.publisher.Mono

class AlimentoIncorretoException(val status: HttpStatus, val resposta: Mono<Resposta>)
    : RuntimeException() {

    constructor(alimento: Alimento) : this(
            HttpStatus.BAD_REQUEST,
            Mono.just(
                    Resposta(
                            Notificacao.ALIMENTO_INCORRETO.codigo,
                            Notificacao.ALIMENTO_INCORRETO.contexto,
                            alimento.toString()))
    )

}
