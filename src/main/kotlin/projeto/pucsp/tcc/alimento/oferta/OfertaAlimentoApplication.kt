package projeto.pucsp.tcc.alimento.oferta

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import projeto.pucsp.tcc.alimento.oferta.propriedade.PropriedadeAlimento
import projeto.pucsp.tcc.alimento.oferta.propriedade.PropriedadeRabbit

@EnableConfigurationProperties(PropriedadeRabbit::class, PropriedadeAlimento::class)
@SpringBootApplication
class OfertaAlimentoApplication

fun main(args: Array<String>) {
    runApplication<OfertaAlimentoApplication>(*args)
}
