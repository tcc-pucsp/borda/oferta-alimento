package projeto.pucsp.tcc.alimento.oferta.entidade

import com.fasterxml.jackson.annotation.JsonProperty
import projeto.pucsp.tcc.alimento.oferta.enumeracao.Notificacao

class Resposta(
        val mensagem: String,
        @JsonProperty("notificacao")
        val respostaNotificacao: RespostaNotificacao) {

    constructor(notificacao: Notificacao) :
            this(
                    notificacao.mensagem,
                    RespostaNotificacao(notificacao.codigo, notificacao.contexto))

    constructor(codigo: Int, contexto: String, mensagem: String)
            : this(mensagem, RespostaNotificacao(codigo, contexto))
}