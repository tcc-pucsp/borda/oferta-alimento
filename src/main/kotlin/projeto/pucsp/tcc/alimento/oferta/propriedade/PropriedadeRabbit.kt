package projeto.pucsp.tcc.alimento.oferta.propriedade

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "rabbit")
class PropriedadeRabbit{

    lateinit var topicExchange: String

}