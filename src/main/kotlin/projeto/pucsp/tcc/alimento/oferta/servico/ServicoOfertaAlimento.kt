package projeto.pucsp.tcc.alimento.oferta.servico

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import projeto.pucsp.tcc.alimento.oferta.core.Recurso
import projeto.pucsp.tcc.alimento.oferta.dominio.AvaliadorAlimento
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import projeto.pucsp.tcc.alimento.oferta.excessao.CodigoAlimentoIncorretoException
import reactor.core.publisher.Mono

@Service
class ServicoOfertaAlimento(@Qualifier("clienteRabbitMQImpl") private val clienteRabbitMQ: Recurso, private val avaliadorAlimento: AvaliadorAlimento) : Recurso {

    override fun publicarOferta(idEmpresarial: Int,
                                emailEmpresarial: String,
                                latitude: String,
                                longitude: String,
                                alimento: Alimento): Mono<Void> {

        return avaliadorAlimento
                .valido(alimento)
                .switchIfEmpty(Mono.error<Alimento>(CodigoAlimentoIncorretoException(alimento.codigo)))
                .flatMap { clienteRabbitMQ.publicarOferta(idEmpresarial, emailEmpresarial, latitude, longitude, alimento) }

    }

}