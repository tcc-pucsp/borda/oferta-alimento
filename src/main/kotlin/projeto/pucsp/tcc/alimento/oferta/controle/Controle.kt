package projeto.pucsp.tcc.alimento.oferta.controle

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import projeto.pucsp.tcc.alimento.oferta.core.Recurso
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/publicar")
class Controle(@Qualifier("servicoOfertaAlimento") private val servico: Recurso) : Recurso {

    companion object {
        val log: Logger = LoggerFactory.getLogger(Controle::class.java)
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    override fun publicarOferta(
            @RequestHeader("id-empresarial") idEmpresarial: Int,
            @RequestHeader("email-empresarial") emailEmpresarial: String,
            @RequestHeader("latitude") latitude: String,
            @RequestHeader("longitude") longitude: String,
            @RequestBody @Validated alimento: Alimento): Mono<Void> {

        log.info("({},{}, {}, {}) Realiza oferta de alimentos : {}", idEmpresarial, emailEmpresarial,
                latitude, longitude, alimento)

        return servico.publicarOferta(idEmpresarial, emailEmpresarial, latitude, longitude, alimento)

    }
}