package projeto.pucsp.tcc.alimento.oferta.enumeracao

import com.fasterxml.jackson.annotation.JsonFormat

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class Notificacao(val codigo: Int, val contexto: String, val mensagem: String) {

    SERVICO_ALIMENTO_INDISPONIVEL(500, "Serviço Indisponível", "Serviço de Alimento indisponível. Tente novamente mais tarde"),
    CODIGO_ALIMENTO_INCORRETO(404, "Código(s) da lista alimento não encontrado(s)", ""),
    ALIMENTO_INCORRETO(4056,
            "Existe(m) incosistência de informações",
            "Alguma informação é incosistente. Verifique os valores e tente novamente")
}
