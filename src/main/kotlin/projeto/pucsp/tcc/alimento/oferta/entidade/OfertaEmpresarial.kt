package projeto.pucsp.tcc.alimento.oferta.entidade

data class OfertaEmpresarial(val emailEmpresarial: String, val idEmpresarial: Int,
                             val alimento: Alimento, val localizacao: Localizacao)