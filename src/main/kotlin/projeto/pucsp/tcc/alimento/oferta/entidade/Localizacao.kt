package projeto.pucsp.tcc.alimento.oferta.entidade

data class Localizacao(val latitude: String, val longitude: String)