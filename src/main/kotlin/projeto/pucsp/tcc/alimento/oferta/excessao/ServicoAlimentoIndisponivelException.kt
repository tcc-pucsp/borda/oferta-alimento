package projeto.pucsp.tcc.alimento.oferta.excessao

import lombok.Getter
import org.springframework.http.HttpStatus
import projeto.pucsp.tcc.alimento.oferta.entidade.Resposta
import reactor.core.publisher.Mono

@Getter
class ServicoAlimentoIndisponivelException(val status: HttpStatus, val resposta: Mono<Resposta>)
    : RuntimeException()
