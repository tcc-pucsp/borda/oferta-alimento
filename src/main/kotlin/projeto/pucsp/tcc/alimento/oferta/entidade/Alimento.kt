package projeto.pucsp.tcc.alimento.oferta.entidade

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.constraints.NotNull

data class Alimento constructor(
        @field:NotNull val codigo: Long,
        @field:NotNull val quantidade: Int,
        @field:NotNull val numeroLote: Int,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        @field:NotNull val dataFabricacao: LocalDate?,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
        @field:NotNull val dataValidade: LocalDate?) {

    fun valido(): Boolean {
        return codigoValido() &&
                numeroLoteValido() &&
                quantidadeAlimentoValida() &&
                datasValidas()
    }

    private fun codigoValido(): Boolean {
        return codigo > 0
    }

    private fun datasValidas(): Boolean {

        val now = LocalDate.now()

        return dataFabricacao!!
                .isBefore(dataValidade)
                .and(dataFabricacao.isBefore(now).or(dataFabricacao.isEqual(now)))
                .and(dataValidade!!.isAfter(now.plusDays(2)))

    }

    private fun numeroLoteValido(): Boolean {
        return numeroLote > 0
    }

    private fun quantidadeAlimentoValida(): Boolean {
        return quantidade > 0
    }
}
