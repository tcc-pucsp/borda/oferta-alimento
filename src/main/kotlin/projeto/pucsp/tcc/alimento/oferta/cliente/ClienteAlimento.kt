package projeto.pucsp.tcc.alimento.oferta.cliente

import org.springframework.web.reactive.function.client.ClientResponse
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import projeto.pucsp.tcc.alimento.oferta.excessao.CodigoAlimentoIncorretoException
import reactor.core.publisher.Mono

interface ClienteAlimento {

    fun obterAlimentoPorCodigo(codigo: Long): Mono<Alimento>

    fun responseError(clientResponse: ClientResponse): Mono<RuntimeException> {
        throw CodigoAlimentoIncorretoException(clientResponse)
    }

}