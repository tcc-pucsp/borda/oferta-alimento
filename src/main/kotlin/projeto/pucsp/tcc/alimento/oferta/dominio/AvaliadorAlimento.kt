package projeto.pucsp.tcc.alimento.oferta.dominio

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import projeto.pucsp.tcc.alimento.oferta.cliente.ClienteAlimento
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import projeto.pucsp.tcc.alimento.oferta.entidade.Resposta
import projeto.pucsp.tcc.alimento.oferta.enumeracao.Notificacao
import projeto.pucsp.tcc.alimento.oferta.excessao.AlimentoIncorretoException
import projeto.pucsp.tcc.alimento.oferta.excessao.ServicoAlimentoIndisponivelException
import reactor.core.publisher.Mono
import reactor.core.publisher.switchIfEmpty
import reactor.core.publisher.toMono
import java.net.ConnectException

@Component
class AvaliadorAlimento(private val clienteAlimento: ClienteAlimento) {

    fun valido(alimento: Alimento): Mono<Alimento> {

        return alimento
                .toMono()
                .filter { alimento.valido() }
                .switchIfEmpty { Mono.error(AlimentoIncorretoException(alimento)) }
                .flatMap {
                    clienteAlimento
                            .obterAlimentoPorCodigo(alimento.codigo)
                            .onErrorResume(ConnectException::class.java)
                            {
                                Mono.error<Alimento>(ServicoAlimentoIndisponivelException(
                                        HttpStatus.INTERNAL_SERVER_ERROR,
                                        Mono.just(Resposta(Notificacao.SERVICO_ALIMENTO_INDISPONIVEL))))
                            }
                            .filter { alimentoRetorno -> alimentoRetorno.codigo == alimento.codigo }
                }

    }
}
