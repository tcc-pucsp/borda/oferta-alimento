package projeto.pucsp.tcc.alimento.oferta.core

import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import reactor.core.publisher.Mono

interface Recurso {

    fun publicarOferta(idEmpresarial: Int, emailEmpresarial: String, latitude: String, longitude: String, alimento: Alimento): Mono<Void>

}