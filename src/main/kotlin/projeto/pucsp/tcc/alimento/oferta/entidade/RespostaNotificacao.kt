package projeto.pucsp.tcc.alimento.oferta.entidade

data class RespostaNotificacao(val codigo: Int, val contexto: String)