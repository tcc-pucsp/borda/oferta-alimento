package projeto.pucsp.tcc.alimento.oferta.excessao

import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.client.ClientResponse
import projeto.pucsp.tcc.alimento.oferta.entidade.Resposta
import projeto.pucsp.tcc.alimento.oferta.enumeracao.Notificacao
import reactor.core.publisher.Mono

class CodigoAlimentoIncorretoException(
        val status: HttpStatus,
        val resposta: Mono<Resposta>) : RuntimeException() {

    constructor(clientResponse: ClientResponse) :
            this(clientResponse.statusCode(),
                    Mono.just(Resposta(Notificacao.CODIGO_ALIMENTO_INCORRETO)))

    constructor(codigo: Long)
            : this(
            HttpStatus.NOT_FOUND,
            Mono.just(
                    Resposta(
                            Notificacao.CODIGO_ALIMENTO_INCORRETO.codigo,
                            Notificacao.CODIGO_ALIMENTO_INCORRETO.contexto,
                            codigo.toString())))

}
