package projeto.pucsp.tcc.alimento.oferta.cliente

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import projeto.pucsp.tcc.alimento.oferta.propriedade.PropriedadeAlimento
import reactor.core.publisher.Mono
import java.net.URI

@Component
class ClienteAlimentoImpl(
        private val propriedadeAlimento: PropriedadeAlimento,
        private val webClient: WebClient) : ClienteAlimento {

    override fun obterAlimentoPorCodigo(codigo: Long): Mono<Alimento> {

        val url = criarUrlString(codigo)

        return webClient
                .get()
                .uri(URI.create(url)).header("Content-Type", "application/json")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::responseError)
                .bodyToMono()
    }

    private fun criarUrlString(codigo: Long): String {

        return String.format("${propriedadeAlimento.servicoUrl}/alimentos/${codigo}")

    }

}