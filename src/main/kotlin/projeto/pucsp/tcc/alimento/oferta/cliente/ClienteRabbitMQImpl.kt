package projeto.pucsp.tcc.alimento.oferta.cliente

import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Component
import projeto.pucsp.tcc.alimento.oferta.core.Recurso
import projeto.pucsp.tcc.alimento.oferta.entidade.Alimento
import projeto.pucsp.tcc.alimento.oferta.entidade.Localizacao
import projeto.pucsp.tcc.alimento.oferta.entidade.OfertaEmpresarial
import projeto.pucsp.tcc.alimento.oferta.propriedade.PropriedadeRabbit
import reactor.core.publisher.Mono

@Component
class ClienteRabbitMQImpl(private val rabbitTemplate: RabbitTemplate, private val propriedadeRabbit: PropriedadeRabbit) : Recurso {

    override fun publicarOferta(idEmpresarial: Int,
                                emailEmpresarial: String,
                                latitude: String,
                                longitude: String,
                                alimento: Alimento): Mono<Void> {

        return Mono.fromRunnable {

            rabbitTemplate.convertAndSend(propriedadeRabbit.topicExchange,
                    OfertaEmpresarial(emailEmpresarial, idEmpresarial, alimento, Localizacao(latitude, longitude)))
        }

    }
}